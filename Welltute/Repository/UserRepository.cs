﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Welltute.Data;
using Welltute.Models;

namespace Welltute.Repository
{

    public class UserRepository
    {
        private WelltuteEntities _ctx;
        public UserRepository()
        {
            _ctx = new WelltuteEntities();
        }
        public async Task<bool> UserRegistration(UserModel model)
        {
            try
            {
                UserMaster objUser = _ctx.UserMasters.FirstOrDefault(x => x.NameOfApplicant == model.NameOfApplicant && x.MobileNo == model.MobileNo);
                if (objUser == null)
                {
                    objUser = new UserMaster()
                    {
                        DateOfBirth = model.DateOfBirth,
                        MobileNo = model.MobileNo,
                        FatherName = model.FatherName,
                        Gender = model.Gender,
                        Location = model.Address,
                        NameOfApplicant = model.NameOfApplicant,
                        RegisteredOn = model.AddedOn,
                    };
                    _ctx.UserMasters.Add(objUser);
                    await _ctx.SaveChangesAsync();

                    UserLocationMaster objLocation = new UserLocationMaster()
                    {
                        ApplicantId = objUser.ApplicantId,
                        Latitude = model.Latitude,
                        Longitude = model.Longitude,
                    };
                    _ctx.UserLocationMasters.Add(objLocation);
                    await _ctx.SaveChangesAsync();

                }
                else
                {
                    objUser.DateOfBirth = model.DateOfBirth;
                    objUser.MobileNo = model.MobileNo;
                    objUser.FatherName = model.FatherName;
                    objUser.Gender = model.Gender;
                    objUser.Location = model.Address;
                    objUser.NameOfApplicant = model.NameOfApplicant;
                    _ctx.SaveChanges();

                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<UserModel> getRegisterUser()
        {
            UserModel model = new UserModel();
            model.Address = "Ahmedabad";
            model.FatherName = "dds";
            model.UserLocationTransaction = await (from a in _ctx.UserLocationMasters
                                                   select new UserLocationTransactionModel
                                                   {
                                                       Latitude = a.Latitude,
                                                       Longitude = a.Longitude,
                                                       NameOfApplicant = a.UserMaster.NameOfApplicant,
                                                       ApplicantId = a.ApplicantId,
                                                       LocationId = a.LocationId
                                                   }).ToListAsync();
            return model;
        }

        public async Task<bool> InsertInquery(InquiryformModel model)
        {
            try
            {
                Inquiryform objInquiry = _ctx.Inquiryforms.FirstOrDefault(x => x.Name == model.Name && x.PhoneNumber == model.PhoneNumber);
                if (objInquiry == null)
                {
                    objInquiry = new Inquiryform()
                    {
                        Name = model.Name,
                        Email = model.Email,
                        PhoneNumber = model.PhoneNumber,
                        Subject = model.Subject,
                        Message = model.Message,
                        Addon = model.Addon,
                    };
                    _ctx.Inquiryforms.Add(objInquiry);
                    await _ctx.SaveChangesAsync();

                }
                else
                {
                    objInquiry.Name = model.Name;
                    objInquiry.Email = model.Email;
                    objInquiry.PhoneNumber = model.PhoneNumber;
                    objInquiry.Subject = model.Subject;
                    objInquiry.Subject = model.Subject;
                    objInquiry.Addon = model.Addon;
                    _ctx.SaveChanges();

                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }


}