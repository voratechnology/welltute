﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;

namespace Welltute.Models
{
    public class UserModel
    {
        public int ApplicantId { get; set; }

        [Required(ErrorMessage = "Applicant Name is required")]
        [StringLength(100, ErrorMessage = "name must be less than 100 charactor")]
        public string NameOfApplicant { get; set; }

        [Required(ErrorMessage = "Father Name is required")]
        [StringLength(100, ErrorMessage = "name must be less than 100 charactor")]
        public string FatherName { get; set; }

        [Required(ErrorMessage = "Date Of Birth is required")]
        public DateTime DateOfBirth { get; set; }

        [Required(ErrorMessage = "Mobile Number is required")]
        public string MobileNo { get; set; }

        [Required(ErrorMessage = "Gender is required")]
        public string Gender { get; set; }

        [Required(ErrorMessage = "Address is required")]
        public string Address { get; set; }

        [Required(ErrorMessage = "Course is required")]
        public string Course { get; set; }

        [Required(ErrorMessage = "Subject is required")]
        public string Subject { get; set; }

        public DateTime AddedOn { get; set; } = DateTime.UtcNow;

        public string Latitude { get; set; }

        public string Longitude { get; set; }
        public List<UserLocationTransactionModel> UserLocationTransaction { get; set; }
        [Required(ErrorMessage = "Mail is required")]
        public string Mail { get; set; }
    }
    public class UserLocationTransactionModel
    {
        public string NameOfApplicant { get; set; }

        public int LocationId { get; set; }

        public int ApplicantId { get; set; }

        public string Latitude { get; set; }

        public string Longitude { get; set; }

    }

    public class InquiryformModel
    {
        public int InquiryID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public DateTime Addon { get; set; } = DateTime.UtcNow;


    }

    public class CommonViewModel
    {
        public UserModel UserModelCV { get; set; }
        public InquiryformModel InquiryformModelCV { get; set; }
    }
}