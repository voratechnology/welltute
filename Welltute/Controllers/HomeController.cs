﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Welltute.Models;
using Welltute.Repository;

namespace Welltute.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        private UserRepository _repo;
        public HomeController()
        {
            _repo = new UserRepository();
        }
        public ActionResult Index()
        {
            //Task<UserModel> result = _repo.getRegisterUser();
            return View();
        }
        [HttpPost]
        public async Task<ActionResult> Registration(CommonViewModel model,string Command)
        {
            if (ModelState.IsValid)
            {
                if (Command == "submit")
                {
                    bool objReg = await _repo.UserRegistration(model.UserModelCV);
                    TempData["Success"] = "True";
                }
                else if(Command == "Send")
                {
                    bool objInq = await _repo.InsertInquery(model.InquiryformModelCV);
                    TempData["Success"] = "True";
                }
            }
            return RedirectToActionPermanent("Index", "Home");
        }
    }
}